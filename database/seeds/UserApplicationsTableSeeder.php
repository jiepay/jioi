<?php

use Illuminate\Database\Seeder;

class UserApplicationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\UserApplication::class, 100)->create();
    }
}
