<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 1)->create([
            'first_name' => "Madara",
            'last_name' => "Uchiha",
            'email' => 'superadmin@jioi.pongo.io',
            'password' => bcrypt('N8WT5srE2te7n3Xs'),
            'role' => 'S',
            'country' => 'mu',
        ]);
        factory(App\User::class, 1)->create([
            'first_name' => "Clark",
            'last_name' => "Kent",
            'email' => 'admin1@jioi.pongo.io',
            'role' => 'A',
            'country' => 'mu',
        ]);
        factory(App\User::class, 1)->create([
            'first_name' => "Bruce",
            'last_name' => "Banner",
            'email' => 'guard1@jioi.pongo.io',
            'role' => 'G',
            'country' => 'mu',
        ]);
        factory(App\User::class, 10)->create();
    }
}
