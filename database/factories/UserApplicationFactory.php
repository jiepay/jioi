<?php

use Faker\Generator as Faker;

$factory->define(App\UserApplication::class, function (Faker $faker) {
    $country = ['mu', 'mg', 'mv', 'km', 'yt', 're', 'sc'][random_int(0, 6)];
    $status = [
        "Minister",
        "President of CIJ",
        "Member of OIC",
        "Member of CIJ",
        "Honorary President of CIJ",
        "International Federation Representative",
        "Guest of Honour",
        "President of National Sports Federation",
        "Head of Delegation",
        "Head of Mission",
        "Responsible of delegation for Men/Women",
        "Judge, Referee, Commissioner",
        "Manager",
        "Coach",
        "Medical Staff",
        "Athlete",
        "Press",
        "Invitee",
    ][random_int(0, 17)];
    $discipline = "";
    if($status == "Athlete"){
        $discipline = [
            "Athletics",
            "Badminton",
            "Basketball",
            "Beach Volleyball",
            "Boxing",
            "Cycling",
            "Football",
            "Weight Lifing",
            "Judo",
            "Swimming",
            "Rugby",
            "Table Tennis",
            "Sailing",
            "Volleyball",
        ][random_int(0, 13)];
    }
    $approved = [true, false][random_int(0, 1)];
    $dateApproved = null;
    if($approved){
        $dateApproved = $faker->date($format = 'Y-m-d', $min = '-3 months', $endDate = 'now', $timezone = 'UTC');
    }
    return [
        'country' => $country,
        'name' => $faker->firstNameMale,
        'last_name' => $faker->lastName,
        'dob' => $faker->date($format = 'Y-m-d', $min = '-30 years', $maz = 'now', $timezone = 'UTC'),
        'gender' => ['M', 'F'][random_int(0, 1)], //Male: 'M', Female: 'F'
        'nationality' => $country,
        'passport_no' => str_random(10),
        'passport_exp' => $faker->date($format = 'Y-m-d', $min = 'now', $max = '+5 years', $timezone = 'UTC'),
        'status' => $status,
        'discipline' => $discipline,
        'approved' => $approved,
        'date_approved' => $dateApproved,
    ];
});
