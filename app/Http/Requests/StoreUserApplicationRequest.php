<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'mimes:jpeg,png',
            'passport' => 'mimes:jpeg,png',
            'name' => 'required|min:2|max:255',
            'last_name' => 'required|min:2|max:255',
            'country' => 'required|size:2',
            'nationality' => 'required|size:2',
            'dob' => 'required|size:10',
            'gender' => 'required|in:M,F',
            'passport_no' => 'required|max:255',
            'passport_exp' => 'size:10',
            'status' => 'required|max:255',
            'discipline' => 'max:255',
        ];
    }
}
