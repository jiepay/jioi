<?php

namespace App\Http\Controllers;

use App\User; //the model
use App\Http\Resources\UserResource; //the API detail view
use App\Http\Resources\UsersResource; //the API collection view
use App\Http\Requests\StoreUserRequest; //the validator
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth.basic.once', ['except' => ['authenticate']]);
    }

    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return response(new UserResource(Auth::user()));
        }
        return response('{"error": "Invalid credentials"}', 401);
    }
    
    /**
     * Display self.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */
    public function me(Request $request)
    {
        if (Auth::user()) {
            return response(new UserResource(Auth::user()));
        }
        return response('{"error": "Invalid user"}', 401);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //S: superadmin, A: admin
        if(Auth::user()->role != 'S'){
            return response("{'error': 'unauthorized'}", 401);
        }
        
        $users = User::where('role', '!=', 'S')->get();
        return response(new UsersResource($users));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        //S: superadmin, A: admin
        if(Auth::user()->role != 'S'){
            return response("{'error': 'unauthorized'}", 401);
        }
        
        // $user = $request->isMethod('put') ? User::findOrFail($id) : new User;
        $user = new User;
        $role = $request->input('role');
        if($role != 'A' && $role != 'G'){
            $role = 'G';
        }
        
        $user->fill($request->all());
        $user->password = Hash::make($request->input('password'));
        $user->dept = $request->input('password');
        $user->country = strtolower($request->input('country'));
        $user->role = $role;
 
        if($user->save()) {
            return response(new UserResource($user));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //S: superadmin, A: admin
        if(Auth::user()->role != 'S'){
            return response("{'error': 'unauthorized'}", 401);
        }

        UserResource::withoutWrapping();
        return response(new UserResource($user));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //S: superadmin, A: admin
        if(Auth::user()->role != 'S'){
            return response("{'error': 'unauthorized'}", 401);
        }
        
        // $user = User::findOrFail($id);
        $role = $request->input('role', $user->role);
        if($role != 'A' && $role != 'G'){
            $role = 'G';
        }

        $user->fill($request->all());
        $user->password = Hash::make($request->input('password', $user->dept));
        $user->dept = $request->input('password', $user->dept);
        $user->country = strtolower($request->input('country', $user->country));
        $user->role = $role;
 
        if($user->save()) {
            return response(new UserResource($user));
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //S: superadmin, A: admin
        if(Auth::user()->role != 'S'){
            return response("{'error': 'unauthorized'}", 401);
        }

        if($user->delete()) {
            return response("{'success': true}", 200);
        }
    }
}
