<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ScanLogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'description' => (string)$this->description,
            'causer' => $this->causer,
            'subject' => $this->subject,
            'datetime' => $this->created_at,
        ];
        // return parent::toArray($request);
    }
}
