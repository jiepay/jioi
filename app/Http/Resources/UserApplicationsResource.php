<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\UserApplicationResource; //the API detail view

class UserApplicationsResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $items = [];
        foreach ($this->collection as $item) {
            array_push($items, new UserApplicationResource($item));
        }
        return [
            // 'data' => $this->collection,
            'applications' => $items,
        ];
        // return parent::toArray($request);
    }
}
