<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\ScanLogResource; //the API detail view

class ScanLogsResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $items = [];
        foreach ($this->collection as $item) {
            array_push($items, new ScanLogResource($item));
        }
        return [
            // 'data' => $this->collection,
            'scanlogs' => $items,
        ];
        // return parent::toArray($request);
    }
}
