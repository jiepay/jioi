<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\UserApplication; //the model
use App\Http\Resources\UserApplicationResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'first_name' => (string)$this->first_name,
            'last_name' => (string)$this->last_name,
            'email' => (string)$this->email,
            'dept' => (string)$this->dept,
            'role' => $this->role,
            'country' => (string)$this->country,
        ];
        // return parent::toArray($request);
    }
}
