<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\UserApplication; //the model
use App\Http\Resources\UserApplicationResource;

class UsersResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $items = [];
        foreach ($this->collection as $item) {
            array_push($items, new UserResource($item));
        }
        return [
            'users' => $items,
        ];
        // return parent::toArray($request);
    }
}
