<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\Activitylog\Traits\LogsActivity; //spatie activity logger

class UserApplication extends Model implements HasMedia{
    use HasMediaTrait;
    use LogsActivity;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'country',
        'name',
        'last_name',
        'photo_crop_x',
        'photo_crop_y',
        'photo_crop_width',
        'photo_crop_height',
        'dob',
        'gender', //Male: 'M', Female: 'F'
        'nationality', //use iso country codes
        'passport_no',
        'passport_exp',
        'status',
        'discipline',
    ];

    protected static $logAttributes = ['*'];
    protected static $logOnlyDirty = true;
}
