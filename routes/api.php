<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@authenticate');
Route::get('me', 'UserController@me');
Route::get('scanlogs', 'UserApplicationController@scanlogs');

Route::resource('users', 'UserController', ['except' => [
  'create', 'edit'
]]);
Route::resource('applications', 'UserApplicationController', ['except' => [
  'create', 'edit'
]]);
