---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## Handle an authentication attempt.

> Example request:

```bash
curl -X POST "http://localhost:8000/api/login" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/login",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/login`


<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_7f3110273f5e3eb6e63b4003183200df -->
## Display self.

> Example request:

```bash
curl -X GET "http://localhost:8000/api/me" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/me",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Invalid user"
}
```

### HTTP Request
`GET api/me`

`HEAD api/me`


<!-- END_7f3110273f5e3eb6e63b4003183200df -->

<!-- START_9b63cbf5c244be892204b12f02c450a6 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://localhost:8000/api/scanlogs" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/scanlogs",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "scanlogs": []
}
```

### HTTP Request
`GET api/scanlogs`

`HEAD api/scanlogs`


<!-- END_9b63cbf5c244be892204b12f02c450a6 -->

<!-- START_da5727be600e4865c1b632f7745c8e91 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://localhost:8000/api/users" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "users": [
        {
            "id": 2,
            "first_name": "Clark",
            "last_name": "Kent",
            "email": "admin1@jioi.pongo.io",
            "dept": "blahblah",
            "role": "A",
            "country": "mu"
        },
        {
            "id": 3,
            "first_name": "Bruce",
            "last_name": "Banner",
            "email": "guard1@jioi.pongo.io",
            "dept": "blahblah",
            "role": "G",
            "country": "mu"
        }
    ]
}
```

### HTTP Request
`GET api/users`

`HEAD api/users`


<!-- END_da5727be600e4865c1b632f7745c8e91 -->

<!-- START_12e37982cc5398c7100e59625ebb5514 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://localhost:8000/api/users" \
-H "Accept: application/json" \
    -d "role"="A" \
    -d "country"="ut" \
    -d "first_name"="ut" \
    -d "last_name"="ut" \
    -d "email"="nakia.mcclure@example.org" \
    -d "password"="ut" \
    -d "application_data"="ut" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users",
    "method": "POST",
    "data": {
        "role": "A",
        "country": "ut",
        "first_name": "ut",
        "last_name": "ut",
        "email": "nakia.mcclure@example.org",
        "password": "ut",
        "application_data": "ut"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/users`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    role | string |  optional  | `S`, `A` or `G`
    country | string |  required  | Must have the size of `2`
    first_name | string |  required  | Minimum: `2` Maximum: `255`
    last_name | string |  required  | Minimum: `2` Maximum: `255`
    email | email |  required  | Maximum: `255`
    password | string |  required  | Minimum: `8` Maximum: `255`
    application_data | string |  optional  | 

<!-- END_12e37982cc5398c7100e59625ebb5514 -->

<!-- START_8f99b42746e451f8dc43742e118cb47b -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://localhost:8000/api/users/{user}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{user}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "id": 1,
    "first_name": "Madara",
    "last_name": "Uchiha",
    "email": "superadmin@jioi.pongo.io",
    "dept": "blahblah",
    "role": "S",
    "country": "mu"
}
```

### HTTP Request
`GET api/users/{user}`

`HEAD api/users/{user}`


<!-- END_8f99b42746e451f8dc43742e118cb47b -->

<!-- START_48a3115be98493a3c866eb0e23347262 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://localhost:8000/api/users/{user}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{user}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/users/{user}`

`PATCH api/users/{user}`


<!-- END_48a3115be98493a3c866eb0e23347262 -->

<!-- START_d2db7a9fe3abd141d5adbc367a88e969 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://localhost:8000/api/users/{user}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/users/{user}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/users/{user}`


<!-- END_d2db7a9fe3abd141d5adbc367a88e969 -->

<!-- START_8684abbddb07d5c9136d8559510cb3e9 -->
## Display a listing of the resource.

> Example request:

```bash
curl -X GET "http://localhost:8000/api/applications" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/applications",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "applications": [
        {
            "id": 1,
            "country": "re",
            "last_name": "Aufderhar",
            "name": "Robb",
            "photo": null,
            "photo_crop_x": 0,
            "photo_crop_y": 0,
            "photo_crop_width": 0,
            "photo_crop_height": 0,
            "dob": "1984-05-29",
            "gender": "F",
            "nationality": "re",
            "passport": null,
            "passport_no": "1H6NjcQ9C2",
            "passport_exp": "1993-09-15",
            "status": "Press",
            "discipline": "",
            "approved": false,
            "date_approved": null,
            "created_at": null
        },
        {
            "id": 2,
            "country": "mv",
            "last_name": "Nicolas",
            "name": "David",
            "photo": null,
            "photo_crop_x": 0,
            "photo_crop_y": 0,
            "photo_crop_width": 0,
            "photo_crop_height": 0,
            "dob": "1977-05-29",
            "gender": "F",
            "nationality": "mv",
            "passport": null,
            "passport_no": "4WO3WmpCqo",
            "passport_exp": "1975-04-20",
            "status": "Coach",
            "discipline": "",
            "approved": false,
            "date_approved": null,
            "created_at": null
        },
        {
            "id": 3,
            "country": "re",
            "last_name": "Rosenbaum",
            "name": "Nasir",
            "photo": null,
            "photo_crop_x": 0,
            "photo_crop_y": 0,
            "photo_crop_width": 0,
            "photo_crop_height": 0,
            "dob": "1984-06-23",
            "gender": "M",
            "nationality": "re",
            "passport": null,
            "passport_no": "7561AlNmMq",
            "passport_exp": "2016-08-05",
            "status": "Manager",
            "discipline": "",
            "approved": true,
            "date_approved": "2016-01-29",
            "created_at": "2016-01-29"
        }
    ]
}
```

### HTTP Request
`GET api/applications`

`HEAD api/applications`


<!-- END_8684abbddb07d5c9136d8559510cb3e9 -->

<!-- START_f4799f4011292e8340c8b711d4802fe1 -->
## Store a newly created resource in storage.

> Example request:

```bash
curl -X POST "http://localhost:8000/api/applications" \
-H "Accept: application/json" \
    -d "photo"="itaque" \
    -d "passport"="itaque" \
    -d "name"="itaque" \
    -d "last_name"="itaque" \
    -d "country"="itaque" \
    -d "nationality"="itaque" \
    -d "dob"="itaque" \
    -d "gender"="F" \
    -d "passport_no"="itaque" \
    -d "passport_exp"="itaque" \
    -d "status"="itaque" \
    -d "discipline"="itaque" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/applications",
    "method": "POST",
    "data": {
        "photo": "itaque",
        "passport": "itaque",
        "name": "itaque",
        "last_name": "itaque",
        "country": "itaque",
        "nationality": "itaque",
        "dob": "itaque",
        "gender": "F",
        "passport_no": "itaque",
        "passport_exp": "itaque",
        "status": "itaque",
        "discipline": "itaque"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/applications`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    photo | string |  optional  | Allowed mime types: `jpeg` or `png`
    passport | string |  optional  | Allowed mime types: `jpeg` or `png`
    name | string |  required  | Minimum: `2` Maximum: `255`
    last_name | string |  required  | Minimum: `2` Maximum: `255`
    country | string |  required  | Must have the size of `2`
    nationality | string |  required  | Must have the size of `2`
    dob | string |  required  | Must have the size of `10`
    gender | string |  required  | `M` or `F`
    passport_no | string |  required  | Maximum: `255`
    passport_exp | string |  optional  | Must have the size of `10`
    status | string |  required  | Maximum: `255`
    discipline | string |  optional  | Maximum: `255`

<!-- END_f4799f4011292e8340c8b711d4802fe1 -->

<!-- START_1fecca750429e499b432fdb1bce93ad7 -->
## Display the specified resource.

> Example request:

```bash
curl -X GET "http://localhost:8000/api/applications/{application}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/applications/{application}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "id": 1,
    "country": "re",
    "last_name": "Aufderhar",
    "name": "Robb",
    "photo": null,
    "photo_crop_x": 0,
    "photo_crop_y": 0,
    "photo_crop_width": 0,
    "photo_crop_height": 0,
    "dob": "1984-05-29",
    "gender": "F",
    "nationality": "re",
    "passport": null,
    "passport_no": "1H6NjcQ9C2",
    "passport_exp": "1993-09-15",
    "status": "Press",
    "discipline": "",
    "approved": false,
    "date_approved": null,
    "created_at": null
}
```

### HTTP Request
`GET api/applications/{application}`

`HEAD api/applications/{application}`


<!-- END_1fecca750429e499b432fdb1bce93ad7 -->

<!-- START_43b521945fb819dc7055dc665a8ef798 -->
## Update the specified resource in storage.

> Example request:

```bash
curl -X PUT "http://localhost:8000/api/applications/{application}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/applications/{application}",
    "method": "PUT",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`PUT api/applications/{application}`

`PATCH api/applications/{application}`


<!-- END_43b521945fb819dc7055dc665a8ef798 -->

<!-- START_56628652c346d61effd39e47e29e4215 -->
## Remove the specified resource from storage.

> Example request:

```bash
curl -X DELETE "http://localhost:8000/api/applications/{application}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost:8000/api/applications/{application}",
    "method": "DELETE",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`DELETE api/applications/{application}`


<!-- END_56628652c346d61effd39e47e29e4215 -->

